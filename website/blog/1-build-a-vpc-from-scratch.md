---
points: 5
type: text
role: cloud
summary: Construct your own Virtual Private Cloud (VPC)
---



## Networking refresher

This video is a good one to review before proceeding ahead with understanding VPC if you are feeling a bit rusty on networking concepts.

## Construct your own Virtual Private Cloud (VPC)

The concept of a cloud (in the IT world) is essentially a pooling of compute, storage, and network resources which can be used for nearly any digital purpose.  It can be deployed to serve a small local client or a huge worldwide customer base and can be adjusted in granular fashion to meet user or business needs.

The cloud began as a bunch of virtual machines hosted by bare metal machines located on the premises of an entity with significant cash flow such as a business, an institution or a government agency.  This is a textbook "private cloud" and it still exists to this day, typically for security and cost at large scale.

But for the last decade or so, the general populace has had access to the _public cloud_, first offered by Amazon and now by an innumerable list of other providers.  The public cloud is open to anyone with an internet connection and is beloved by hobbyists, researchers, and small-to-medium businesses.  But what if you wish to join several cloud assets together in a secure environment where they can cooperate to meet a business goal?  This is the very role of a _virtual private cloud_ or VPC.

In this lesson I will walk you through the setup and configuration of a VPC on Amazon AWS.

###  Amazon Web Services

Amazon Web Services, or AWS, provides an easy VPC wizard to set up your own "private cloud" but I think you'll learn a whole lot more about the fundamentals by building your VPC piece by piece.

Open the [Amazon AWS][] page, select **Sign In to the Console** and log in with your IAM credentials.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-console-sign-in.png)

Select the region nearest to you geographically as it will provide you with the best experience and quickest response times when deploying your VPC.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-console-region-select.png)

You will use the EC2 Compute and the VPC Networking & Content Delivery services throughout this exercise.  As such, you will eventually see these under your _Recently visited services_ like so

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-console-services.png)

For now, type in `EC2` in the upper _AWS services_ search box and click **EC2** in the search results.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-console-services-ec2.png)

You will be presented with the EC2 dashboard which shows a summary of your compute resources, complete with health check status and scheduled events.

Notice in the top right that there is a preset Default VPC.  If you create a compute instance it will be associated with that VPC by default.  There is also 1 security group already created, also a default.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-ec2-dashboard.png)

Do not remove the defaults-- later we will tag them with 'ignore' and create our own.

## Establish Your VPC

The first element needed to create a VPC is... a VPC.  While not strictly mandatory, defining your VPC first will simplify the rest of the process because all of the other elements that we create will be directly or indirectly associated to this VPC.  The VPC acts a self-contained network and is analagous to a home network managed by your home router.

Click on **Services** at the top of the screen and type `VPC` in the search field.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-console-services-vpc.png)

Once you have visited a service dashboard you can use the side _History_ navigation bar to switch services quickly.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-console-services-vpc-history.png)

Click on the **1 VPC** shown in the VPC dashboard to view it.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-view.png)

You will see the default VPC listed.  Select the row and click on the _Name_ field.  Enter `Ignore for now` and click the check mark.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-default-rename.png)

Click **Create VPC**.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-create.png)

Enter a name for the VPC and type in a wide [CIDR][] block, for example `10.0.0.0/16`.  This will reserve addresses from `10.0.0.0` to `10.0.255.255` for your VPC.  You can choose any IP range, with the exception of some [reserved ranges][] such as `127.0.0.0` (loopback) and `0.0.0.0` (local broadcast).  You can specify a netmask (think size) as big as /16 (65,536 addresses) and as small as /28 (16 addresses).

Click **Yes, Create**.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-create-1.png)

You now have your VPC element.

###  Tag the VPC Default Security Group

We will address security groups more after launching a VM, but for now let's tag the default security group that was automatically created along with your new VPC.

Click **Security Groups** in the side nav bar.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-security-group-default-tag-1.png)

Select the security group that is associated to the _VPC-CloudSeminar_ VPC

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-security-group-default-tag-2.png)

Rename it to _SG-CloudSeminar-Default_.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-security-group-default-tag-3.png)

## Create a Subnet

Within your VPC you will have one or more subnets with which to organize your VM instances.

Click on **Subnets** in the left navigation bar.  You will see three default subnets, one for each availability zone in the selected region.  Notice that they are all associated to the "Ignore for now" default VPC.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-subnet-view.png)

Enter the name, "Ignore this subnet", for each of these.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-subnet-ignore-defaults.png)

Click **Create Subnet**

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-subnet-create.png)

Enter `SN-Public` for the subnet's name tag as we will hook up this subnet directly to an internet gateway.  Select the _VPC-CloudSeminar_ VPC.  Enter a CIDR block of `10.0.0.0/24` to reserve the 256 addresses between `10.0.0.0` and `10.0.0.255`.  The subnet CIDR block can be as large as, but not larger than, the VPC it is created in.

In larger production deployments you may want to have a subnet for each availability zone within a region to better serve your clients.  However for this exercise we will select _No preference_.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-subnet-create-21.png)

Create another subnet named `SN-Private` and assign it to CIDR block `10.0.1.0/24` to reserve IP addresses `10.0.1.0` through `10.0.1.255`.  We will later connect this subnet to a [NAT][] box in the _SN-Public_ subnet, giving it access to the internet indirectly.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-subnet-create-31.png)

You will now see your two new subnets along with the three defaults.  Let's organize these subnets so we can focus only on the ones for this exercise.  Type `CloudSeminar` in the search box directly above the list of subnets.  This will filter for items with `CloudSeminar` in the name, in a tag or associated with a VPC with that text.

> _Note: AWS has a neat feature where your filters will persist even if you navigate away from a particular view.  When you return your filter will remain active.  Keep in mind you will have to clear the search box to view or manage any objects excluded by the filter._

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-subnet-view-filter1.png)

## Create a Key Pair

You will typically use SSH to remotely connect to an instance in the cloud.  Recall in an earlier lesson how we used public and private keypairs to securely connect with SSH.  In order to deploy an instance with our public key we will import a key into AWS and select it when creating instances.

Run this command in a terminal on your local machine.  Skip the passphrase.
~~~
$ CMD_BEGIN ssh-keygen -b 4096 -t rsa -f ~/.ssh/aws-vpc CMD_END

Generating public/private rsa key pair.
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in aws-vpc.
Your public key has been saved in aws-vpc.pub.
The key fingerprint is:
SHA256:PO27wbYFgUOdKhOhczDZNWV4+/sYkhXisonuEuS8xO4 user@cg
The key's randomart image is:
+---[RSA 4096]----+
|    oo.o++o.     |
|    .+o..++      |
|    o ..ooo..    |
|    .oo..+.o .   |
|   =   oS +..    |
|    *  . B +.    |
|   o o. o B o.   |
|    +.   . *.o   |
|   .Eoo   +....  |
+----[SHA256]-----+
~~~

Change the access permissions to read-only.
~~~
$ CMD_BEGIN chmod 400 ~/.ssh/aws-vpc CMD_END
~~~

Click **Key Pairs** on the _EC2 Dashboard_.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-ec2-key-pairs.png)

Click **Import Key Pair**.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-ec2-key-pairs-2.png)

Click **Choose file** and pick the `aws-vpc.pub` file.  The contents of the public key will appear in the text box.

Click **Import**.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-ec2-key-pairs-3.png)

Your imported key will appear on the _Key Pairs_ dashboard.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-ec2-key-pairs-4.png)

## Launch Your First Instance

Your new VPC won't be able to do much without a machine running in it.  Much of the functionality you might want from a cloud application will be hosted on a compute instance-- a web server, a backend database, a logging/monitoring suite, etc.

Begin creating an instance by clicking **Launch Instance** on the _EC2 Dashboard_.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-ec2-launch-instance.png)

The first screen allows you to select the OS image to load on your instance.  Amazon calls them _AMI_ for _Amazon Machine Image_.  Each AMI has it's benefits and drawbacks and some are even curated by third parties who charge usage fees to deploy them.  For our purposes the free-tier Amazon Linux and Ubuntu AMIs are sufficient.

Click **Select** next to the top _Amazon Linux AMI_.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-ec2-launch-instance-2.png)

Choose the free tier _t2.micro_ instance type and click **Next: Configure Instance Details**.  Other types are available with more memory, storage and processing power but will cost a significantly more and aren't necessary for this demonstration.

> _Note: Recall that the free tier will permit you 750 hours per month of free-tier eligible compute instances. This equates to 1 instance running continuously for 1 month, 2 instances running for half a month, 5 instances running for 150 hours per month, etc.  The t2.micro is the only free-tier eligible instance type._

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-ec2-launch-instance-3.png)

The _Configure Instance Details_ screen has a lot to digest.  Let's break it down:

* **Number of instances:** You may with to deploy several copies of this machine at once for a production application.  Select `1` instance.
* **Purchasing option:** [Spot instances][] are cheaper but may not always be available.  Keep this option unchecked.
* **Network:** This option sets the VPC network that the instance will be deployed into.  Select `VPC-CloudSeminar`.
* **Subnet:** Likewise, this option sets the subnet that your VM will be associated with.  Choose the `SN-Public` subnet.
* **Auto-assign Public IP:** A public IP address will permit direct access from anywhere on the internet, granted your security group settings permit it.  Without a public IP address, only instances within the VPC will be able to directly access the instance; access from outside will require "jumping" through a publicly-accessible instance which we will demonstrate later.  Choose `Enable`.
* **IAM role:** The IAM role setting is different but similar to the IAM account that you set up in the second prep lesson.  This is used when you want to grant management control to other users or other AWS services.  Keep this selected to `None`.
* **Shutdown behavior:** When you choose to shut down an instance you can have it merely "turned off" with the _Stop_ option, or have it destroyed with the _Terminate_ option.  Choose `Stop`.
* **Enable termination protection:** You can put a safety on the "power button" of your instance with this option.  It requires an administrator to first disable the protection before initiating instance termination.
* **Monitoring:** AWS offers the CloudWatch service to gather detailed monitoring metrics on your instance.  Keep this option unchecked.
* **Tenancy:** For an additional cost you can have your instance hosted on it's very own hardware not shared with other users' instances.  Select `Shared - Run a shared hardware instance` here.
* **T2 Unlimited:** This option permits your t2.micro to sustain higher-than-budgeted computational workload for an additional flat rate.  Users might choose this option to squeeze as much as possible out of their t2 instances.  Keep this unchecked.

Click **Next: Add Storage**.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-ec2-launch-instance-4.png)

The next screen permits you to choose the type and size of storage you would like mounted in your instance.  You can keep the default of 8GB of general purpose SSD.  A provisioned IOPS SSD allows you to tune the I/O operations to boost storage performance at an increased cost.  Although magnetic is available it is not desirable as there is a performance reduction and no cost savings.

Click **Next: Add Tags**.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-ec2-launch-instance-5.png)

Add a _Name_ tag to this instance.  Click the **click to add a Name tag** link.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-ec2-launch-instance-6.png)

Enter `JumpBox` for the instance name.  Click **Next: Configure Security Group**.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-ec2-launch-instance-7.png)

A security group is like a mini firewall that you can wrap around your instances to control access into and out of the instances.  You will create a security group for _JumpBox_ that permits SSH connections from your IP only.

* **Assign a security group:** _Create a new security group_
* **Security group name:** `SG-JumpBox`
* **Description:** `Permit SSH inbound from my IP`
* **Type:** _SSH_
* **Source:** _My IP_

Leave the rest of the settings and click **Review and Launch**.

> _Note:  You can add a description for each security group rule, such as the SSH rule, however since we are only using one rule in this SG we can skip this step.  When using more complex SGs it's a good practice to annotate each rule's purpose._

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-ec2-launch-instance-8.png)

Click **Review and Launch**.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-ec2-launch-instance-9.png)

You are given a chance to review the details of your instance launch before clicking the **Launch** button.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-ec2-launch-instance-10.png)

The final step before launch is to select a keypair to be loaded on the VM for SSH access.  Recall we imported a key pair earlier into AWS.

Select _Choose an existing key pair_ and _aws-vpc_.  Click the checkbox to acknowledge that the private key file is irreplaceable and if it is lost, not even an AWS admin will be able to access the virtual machine instance.

Click **Launch Instances**.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-ec2-launch-instance-11.png)

AWS presents you with a helpful page with links to the launch log, billing alarm setup, and learning resources.  Click **View Instances**.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-ec2-launch-instance-12.png)

From the instances dashboard you can quickly find status and attributes for your instances.  After a few minutes you should see an _Instance State: running_ and _Status Checks: 2/2 checks passed_.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-ec2-instance-dashboard1.png)

Under the _Description_ tab, check the _IPv4 Public IP_ and the _Private IP_.  Note that the private IP for this machine is `10.0.0.237` which falls within the _SN-Public_ subnet of `10.0.0.0/24`.

Let's try to connect to the machine with ssh.  Make sure to use the _IPv4 Public IP_.
~~~
$ CMD_BEGIN ssh -i ~/.ssh/aws-vpc ec2-user@54.191.80.107 CMD_END
~~~

You may be surprised that the ssh command hangs with no connection or even a refusal reported.  Hit _Ctrl-C_ to break and return to the command line.

The reason for this is that the VM is isolated from the internet within our new VPC.  What we need is a gateway to link our VPC to the larger public internet.

Navigate back to **Services**, **VPC** and click **Internet Gateways**.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-internet-gateway.png)

There is already a gateway associated to the default VPC.  Name it `Ignore for now` and click **Create Internet Gateway**.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-internet-gateway-2.png)

Name the new internet gateway `IG-CloudSeminar` and click **Yes, Create**.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-internet-gateway-3.png)

The new gateway shows a _State: detached_.  Click **Attach to VPC**.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-internet-gateway-4.png)

Select _VPC-CloudSeminar_ and click **Yes, Attach**.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-internet-gateway-5.png)

The VPC now has a link to the public internet, but you'll still find that attempting an ssh connection fails.  That's because our new instance needs a pathway, or a route, out to the internet through the gateway.

This may be a bit confusing at first.  The analogy of a home network should help:  the gateway we setup and associated with the VPC is similar to when you buy a home router and plug it into your internet provider's modem.  The next step we will take is to edit the route tables and add a route from the _SN-Public_ and _SN-Private_ subnets; in terms of the analogy I'm using, this is like hooking your computer up to the home router with a cable.  This analogy falls apart if you start taking it too far, but I hope this gets the point across.

Click **Route Tables**.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-route-tables.png)

You will see two route table entries: one associated to your default VPC and the other tied to _VPC-CloudSeminar_.  Name the former `Ignore for now` and then name the latter `RT-Public`.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-route-tables-2.png)

With _RT-Public_ selected, click on the **Subnet Associations** tab.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-route-tables-31.png)

A message indicates that you do not have any _explicit_ subnet associations to this _RT-Public_ route table.  However, since this is the primary route table for _VPC-CloudSeminar_ it becomes the default route table for the subnets under the same VPC, namely _SN-Public_ and _SN-Private_.

Let's define the relationship more clearly.  Click **Edit**.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-route-tables-41.png)

Put a check mark next to the _SN-Public_ subnet and click **Save**.  Since this route table hooks directly to the internet gateway and hence the internet we only want machines in the _SN-Public_ of our deployment to be routed according the rule we add in the next step.  We will add another route table later for the _SN-Private_ subnet when we have a NAT box up and running to connect to.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-route-tables-51.png)

Click on the **Routes** tab.  One route rule is already in place which says to direct any requests for addresses between `10.0.0.0` and `10.0.255.255` (our _SN-Public_ subnet) to the local subnet.  This is a reflexive type of rule that permits any two machines on a common subnet to reach each other.  AWS will not permit this rule to be modified or deleted, as doing so would break basic VPC functionality.

> _Note:  AWS uses longest-prefix matching to determine which route table rule is used for outbound traffic.  This means that the most specific rule that best matches the intended destination address will be used.  Read more on the [AWS VPC Route Tables][] documentation._

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-route-tables-61.png)

Click **Edit**.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-route-tables-71.png)

Then **Add another route**.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-route-tables-81.png)

For the new rule, enter a destination CIDR block of `0.0.0.0/0` which equates to all IPv4 addresses.  Once the cursor is in the _Target_ field, a suggestion will appear showing the _IG-CloudSeminar_ internet gateway.  Click this suggestion and then **Save** the route rules.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-route-tables-91.png)

We have now established a publicly routable, two-way communication channel for our first compute instance and any other instance with a public IP that we place within the _SN-Public_ subnet.

Test it by connecting to your _JumpBox_ with ssh.  You should see a host key verfication asking if you trust the remote machine.  Answer `yes` and you will see the Amazon Linux AMI welcome ASCII art and a bash command line customized with the `ec2-user` username and machine's private IP.

~~~
$ CMD_BEGIN ssh -i ~/.ssh/aws-vpc ec2-user@54.191.80.107 CMD_END

The authenticity of host '54.191.80.107 (54.191.80.107)' can't be established.
ECDSA key fingerprint is SHA256:Am7HnIZTJArgFTse7EtTBVETfIsXYbzGqIOsJDaC5IU.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added '54.191.80.107' (ECDSA) to the list of known hosts.

       __|  __|_  )
       _|  (     /   Amazon Linux AMI
      ___|___|___|

https://aws.amazon.com/amazon-linux-ami/2017.09-release-notes/

ec2-user@ip-10-0-0-237:~$
~~~

At any time you can log off the remote machine by typing `exit` at a command prompt.  The command prompt returns to the usual `user@cg`.
~~~
ec2-user@ip-10-0-0-237:~$ CMD_BEGIN exit CMD_END
logout
Connection to 54.191.80.107 closed.

user@cg:~$
~~~

## Deploy a NAT Box

A good practice for security in cloud architecture is to place all of your backend applications on machines behind a NAT box and without a public IP.  The only direct connections to these backend hosts should come from within your VPC.  AWS offers a plug-in NAT that deploys in a similar fashion as an internet gateway, but I'd like to show you how to set up your own NAT using a simple VM.

Apart from the price difference, by learning how to set up and manage a NAT device of your own will allow you to operate VPCs in different cloud providers as not all providers offer a NAT like AWS does.

> _Note: AWS charges $0.045 per hour for a NAT gateway which comes out to over $30 per month.  Bandwidth also adds cost at $0.045 per GB transferred.  Performance aside, the t2.micro we will deploy costs just over $5 per month (once the free tier allowance is exceeded/expired)._

Create a new instance but choose an image that is built to operate as a NAT.  After **Launch Instance**, scroll down the list of AMIs and **Select** the _Ubuntu Server 16.04 LTS (HVM), SSD Volume Type_.

> What is the difference between `Amazon Linux AMI` and `amzn-ami-vpc-nat...` found in the Community AMI section?

> `Amazon Linux AMI` is Amazon's Linux distribution.

> `amzn-ami-vpc-nat...` has NAT function added to Amazon's Linux distribution. This makes it an image that is built to operate as a NAT.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-ec2-nat-box-ami-select-1.png)

Continue creating the instance with the rest of the settings the same as the first VM:

* **Machine:** _t2.micro_
* **Subnet:** _SN-Public_
* **Auto-assign Public IP:** _Enable_
* **Tags:** `Name = NAT`
* **Security Group:** _SG-JumpBox_
* **Key pair:** _vpc-aws_

After launch return to the EC2 instance dashboard.  For up to several minutes you will see an _instance state: pending_ and _status checks: initializing_.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-ec2-nat-box-launching1.png)

We will temporarily use the _SG-JumpBox_ security group to verify connectivity with the box after launch.  As this rule permits SSH from your IP you should be able to quickly connect with the following command.  Be sure to copy the IPv4 Public IP from the instance description and note that the user is `ubuntu`.
~~~
$ CMD_BEGIN ssh -i ~/.ssh/aws-vpc ubuntu@34.213.185.195 CMD_END
~~~

If you saw the host key verification question and were able to connect, then you can rest assured the machine has launched successfully.  Your VPC is starting to take shape.

Try pinging the private IP address of the _JumpBox_ instance.  You should not see any response since the _SG-JumpBox_ security group does not permit inbound ICMP (ping) packets.  Hit _Ctrl-C_ to end the ping operation and `exit` the ssh connection.
~~~
$ CMD_BEGIN ping 10.0.0.237 CMD_END
PING 10.0.0.237 (10.0.0.237) 56(84) bytes of data.
~~~

Before we hook up a new machine in the _SN-Private_ subnet and connect it to the _NAT_ box with a route table, we will need to get our instances talking with each other.  I will also demonstrate how to isolate the _NAT_ box from outside SSH connections and use the _JumpBox_ as a proxy agent.  This will be our template for all future instances in the VPC.

On the _Instances_ view, select the _NAT_ instance.  Click **Actions**, **Networking**, **Change Security Groups**.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-ec2-nat-box-security-groups-11.png)

Deselect the _SG-JumpBox_ security group and pick the _default_ security group.  Click **Assign Security Groups**.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-ec2-nat-box-security-groups-2.png)

Similarly, select the _JumpBox_ instance and add the _default_ security group while keeping the _SG-JumpBox_ security group.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-ec2-nat-box-security-groups-3.png)

Click on **Security Groups** in the left nav bar and select the _SG-CloudSeminar-Default_ security group.  Notice the inbound filter permits all traffic from within the default group.  This makes a convenient way to permit your boxes to talk to each other.  If you add this security group to a instance it can communicate with any other instance also associated with this security group.

SSH in to your _JumpBox_ and ping the private IP of the _NAT_.
~~~
user@cg:~$ CMD_BEGIN ssh -i ~/.ssh/aws-vpc ec2-user@54.191.80.107 CMD_END
~~~
~~~
ec2-user@ip-10-0-0-237:~$ CMD_BEGIN ping 10.0.0.225 CMD_END
PING 10.0.0.225 (10.0.0.225) 56(84) bytes of data.
64 bytes from 10.0.0.225: icmp_seq=1 ttl=255 time=0.697 ms
64 bytes from 10.0.0.225: icmp_seq=2 ttl=255 time=0.395 ms
64 bytes from 10.0.0.225: icmp_seq=3 ttl=255 time=0.870 ms
...
~~~

Ok, so we can get our instances talking with each other but how does one SSH into an instance that has a security rule preventing outside SSH access?  The SSH toolkit has a feature called `ProxyCommand`  that lets you use a reachable instance as a proxy to relay the SSH connection to any instance the proxy can reach.  This is the main purpose of a _JumpBox_, otherwise known as a _Bastion_.

In a terminal on your local machine, edit the SSH config file `~/.ssh/config`

Put this data into the config file, changing the `HostNames` to match the public IP for your _JumpBox_ and the private IP for your _NAT_.  Save and close the file.
~~~
CMD_BEGIN Host JumpBox
    HostName 54.191.80.107
    User ec2-user
    IdentityFile /home/user/.ssh/aws-vpc
    StrictHostKeyChecking ask  CMD_END
~~~
~~~
CMD_BEGIN Host NAT
    HostName 10.0.0.225
    User ubuntu
    IdentityFile /home/user/.ssh/aws-vpc
    StrictHostKeyChecking ask
    Port 22
    ProxyCommand ssh -q -W %h:%p JumpBox CMD_END
~~~

Here's a brief explanation of the options:

* **Host:** A convenient alias for the remote machine.  You can now use type this name instead of the IP address.
* **HostName:** The IP address of the box.
* **User:** The username you use to log in.
* **IdentityFile:** The private key.
* **StrictHostKeyChecking:** Setting this to `ask` instructs SSH to let you review new host key fingerprints before adding them to the `~/.ssh/known_hosts` file; it also refuses to a host whose key has changed.
* **Port:** Specifies the port to use for the proxy command.  This is typically `22`.
* **ProxyCommand:** This instructs SSH to use _JumpBox_ as a proxy when connecting to _NAT_.

Try connecting to your _JumpBox_ with this command.  So much easier!
~~~
$ CMD_BEGIN ssh JumpBox CMD_END
~~~

Likewise, connect to your _NAT_.  Joy!
~~~
$ CMD_BEGIN ssh NAT CMD_END
~~~

And while you are logged on to your _NAT_, we should make sure our internet connection is working.  Ping an arbitrary internet site.
~~~
$ CMD_BEGIN ping google.com CMD_END

PING google.com (216.58.216.142) 56(84) bytes of data.
64 bytes from sea15s01-in-f14.1e100.net (216.58.216.142): icmp_seq=1 ttl=46 time=23.8 ms
64 bytes from sea15s01-in-f142.1e100.net (216.58.216.142): icmp_seq=2 ttl=46 time=23.9 ms
64 bytes from sea15s01-in-f14.1e100.net (216.58.216.142): icmp_seq=3 ttl=46 time=23.9 ms
...
~~~

We will also need to configure the Ubuntu installation on our _NAT_ to properly forward traffic in masquerade mode.  Copy this code into the terminal and execute it.  Make sure to copy **all** of the code in the block.

> _Note: If you choose a different CIDR block for your VPC you will have to change `10.0.0.0/16` to match what you chose._
~~~
$ CMD_BEGIN echo '#!/bin/sh
echo 1 > /proc/sys/net/ipv4/ip_forward
iptables -t nat -A POSTROUTING -s 10.0.0.0/16 -j MASQUERADE
echo "NAT ROUTING ENABLED"
' | sudo tee /etc/network/if-pre-up.d/nat-setup  CMD_END
~~~

Followed by this command:
~~~
$ CMD_BEGIN sudo chmod +x /etc/network/if-pre-up.d/nat-setup CMD_END
~~~

And this command:
~~~
$ CMD_BEGIN sudo /etc/network/if-pre-up.d/nat-setup CMD_END
~~~

You should see the text `NAT FORWARDING ENABLED` if this executed successfully.  You can ignore the complaints about `sudo: unable to resolve host ip-XX-XX-XX-XX`.

Log off the _NAT_ box.

> ## Debugging Note:
> Recent Linux distributions seem to have updated `iptables` that requires explicit reference of the network adapter being used. Like so:
>
> New
> `sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE`
> Old
> `sudo iptables -t nat -A POSTROUTING -j MASQUERADE`

The final configuration step for the _NAT_ takes place on the _EC2_ dashboard.  The _Source/Dest Check_ should be toggled off since the _NAT_ box will be relaying traffic in and out of the VPC while handling address translation.

Select the _NAT_ instance, then right-click and select **Networking**, **Change Source/Dest. Check**.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-ec2-nat-box-source-dest-check1.png)

Click **Yes, Disable**.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-ec2-nat-box-source-dest-check-1.png)

At this point you have the following operational:

* A jump box that is a single point of entry for SSH administration of any machine in your VPC
* A NAT box that has access to the world wide web


## Understanding NAT

> Think of the NAT as the common mailroom in a large apartment building. All outbound & inbound mails must go through the dedicated mailbox in the mailroom.
>
> * Imagine you live in one of the apartments. When you want to mail out a letter, you write the address you are tying to send to as well as your return address on the envelope. You will then place this envelope in the mailbox.
>
> * When receipient replies to your mail, they know where to send it based on your return address which also has the apartment unit number. The mail will arrive in the common mailbox and not directly to your doorstep.
>
> NAT serves a similar purpose for private networks.
>
> * All outbound traffic from host in a private network will go through a NAT thus allowing the private machines to communicate with internet just using a private IP.
>
> * This enhances the security of your nodes by not revealing a public IP of the node. The NAT will know how to transfer outbound and inbound packets based on internal lookup tables and will securely deliver the packets.

> more at https://www.karlrupp.net/en/computer/nat_tutorial

## The Backend Database

We have a good infrastructure set up-- now let's deploy an application server.

Create a new instance with these selections.  Note you are not assigning a public IP address and the instance will be in the _SN-Private_ subnet.

* AMI: _Ubuntu Server 16.04 LTS_
* Machine: _t2.micro_
* Subnet: _SN-Private_
* Auto-assign Public IP: _Disable_
* Tags: _Name = Database_
* Security Group: _default_
* Key pair:  _aws-vpc_

Edit the `~/.ssh/config` file on your local machine and add the _Database_ host.
~~~
Host Database
    HostName 10.0.1.93
    User ubuntu
    IdentityFile /home/user/.ssh/aws-vpc
    Port 22
    ProxyCommand ssh -q -W %h:%p JumpBox
~~~

If you SSH into this instance you will find that you can ping other instances in your VPC by their private IP address but you cannot reach the internet.  This is where the _NAT_ box comes in.

Navigate to **VPC Dashboard**, **Route Tables** and click **Create Route Table**.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-route-tables-back-1.png)

Name the route table `RT-Private` and select the _VPC-CloudSeminar_.  Click **Yes, Create**.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-route-tables-back-2.png)

Edit the routes and add a line with a destination of `0.0.0.0/0` and a target of your _NAT_.  Click **Save**.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-route-tables-back-31.png)

Now to link up the lonely _SN-Private_ subnet to this routing table.  Under _Subnet Associations_, click **Edit**.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-route-tables-back-41.png)

Check the box next to the _SN-Private_ subnet and click **Save**.

![alt-name](https://cloudgenius.s3.amazonaws.com/images/lesson-09_amazon-aws-vpc-route-tables-back-51.png)

SSH into the _Database_ instance.
~~~
$ CMD_BEGIN ssh Database  CMD_END
~~~

And ping an external site
~~~
ubuntu@ip-10-0-1-93:~$ CMD_BEGIN ping cnn.com CMD_END

PING cnn.com (151.101.1.67) 56(84) bytes of data.
64 bytes from 151.101.1.67: icmp_seq=1 ttl=48 time=9.19 ms
64 bytes from 151.101.1.67: icmp_seq=2 ttl=48 time=9.11 ms
64 bytes from 151.101.1.67: icmp_seq=3 ttl=48 time=9.30 ms
^C
--- cnn.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 9.119/9.206/9.307/0.077 ms
~~~

[amazon aws]:               https://aws.amazon.com                                                                       "https://amazonaws.com"
[spot instances]:           http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/how-spot-instances-work.html             "http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/how-spot-instances-work.html"
[cidr]:                     https://www.lifewire.com/cidr-classless-domain-routing-818375                               "https://www.lifewire.com/cidr-classless-domain-routing-818375"
[reserved ranges]:          https://en.wikipedia.org/wiki/Reserved_IP_addresses                                         "https://en.wikipedia.org/wiki/Reserved_IP_addresses"
[nat]:                      https://en.wikipedia.org/wiki/Network_address_translation                                                           "https://en.wikipedia.org/wiki/Network_address_translation"
[aws vpc route tables]:     https://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_Route_Tables.html                "https://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_Route_Tables.html"
[aws vpc pricing]:          https://aws.amazon.com/vpc/pricing/                                                         "https://aws.amazon.com/vpc/pricing/"
